<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, X-CID');

if ($_SERVER['REQUEST_METHOD'] !== "POST") {
    return;
}

$body = file_get_contents('php://input');

$headers = [
    "Host: i4api.spse.cz",
    "User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/118.0",
    "Accept: */*",
    "Content-Type: application/json; charset=utf-8",
    "X-CID: " . $_SERVER["HTTP_X_CID"],
    "Origin: https://i4.spse.cz",
    "Referer: https://i4.spse.cz/",
    "Sec-Fetch-Dest: empty",
    "Sec-Fetch-Mode: cors",
    "Sec-Fetch-Site: same-site",
    "TE: trailers"
];

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL,"https://i4api.spse.cz/entry.php");
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
curl_exec($ch);
curl_close($ch);

?>
